# App README

Kurze Beschreibung der App / des Programms / Frameworks etc.

## Setup

Erklärung, wie man die App zum  laufen bringt.
Welche Voraussetzungen erfüllt sein müssen (API-Keys, API-Level) 
und wie man diese eventuell erfüllt.
Dazu eventuelle Quellen angeben.

## Permissions

Genutzte Permissions.

## Guideline

Erklärung der einzelnen Menüpunkte in der App.

## Erweitere Einstellungsmöglichkeiten

Erklärung von Besonderheiten bzw. Einstellmöglichkeiten in der App.
Z.B.wie ein Suchmaske oder ein Filter angepasst werden kann.
Erklärung der einzelnen Einstelloptionen. Z.B. bei Audioaufnahmen 
verschiedene Qualitäten. 

## Anmerkungen

Vorschläge für neue Funktionen. Bugfixes oder sonstiges.

## Support / Autor

Wo man den Autor bzw. Support erreichen kann.